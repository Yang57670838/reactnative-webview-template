import { StyleSheet, Text, View, Button, TextInput } from "react-native";
import { useState } from "react";

export default function App() {
  const [formId, setFormId] = useState();
  function formIdChangeHandler(value) {
    setFormId(value);
  }

  function startForm() {
    // redirect to web view
    console.log("formId", formId);
  }

  return (
    <View style={styles.appContainer}>
      <View>
        <Text>Welcome</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput placeholder="Enter Form ID" style={styles.textInput} onChangeText={formIdChangeHandler}>Enter form ID</TextInput>
        <Button title="Start" onPress={startForm} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  appContainer: {
    paddingTop: 50,
    paddingHorizontal: 16,
  },
  inputContainer: {
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc"
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#cccccc",
    width: "80%",
    padding: 8
  }
});
